//
//  ViewController.swift
//  SearchStoreApp
//
//  Created by Mohammad Usama on 23/08/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
        
        var searchResults = [SearchResult]()
        var hasSearched = false
        var isLoading = false
        var dataTask: URLSessionDataTask?
    
        
       struct TableView {
            struct CellIdentifiers {
                static let searchResultCell = "SearchResultCell"
                static let nothingFoundCell = "NothingFoundCell"
                static let loadingCell = "LoadingCell"
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            tableView.contentInset = UIEdgeInsets(top: 108, left: 0, bottom: 0, right: 0)
            
            //Xib Cell For SearchResults:-
            var cellNib = UINib(nibName: TableView.CellIdentifiers.searchResultCell, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.searchResultCell)
           
            //Xib Cell For Nothing Found:-
            cellNib = UINib(nibName: TableView.CellIdentifiers.nothingFoundCell, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.nothingFoundCell)
            
            //Nib Cell For LoadingCell:-
            cellNib = UINib(nibName: TableView.CellIdentifiers.loadingCell, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.loadingCell)
            
            //Showing Keyboard:-
            searchBar.becomeFirstResponder()
            
            let segmentColor = UIColor(red: 10/255, green: 80/255, blue: 80/255, alpha: 1)
            let selectedTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            let normalTextAttributes = [NSAttributedString.Key.foregroundColor: segmentColor]
           
            segmentedControl.selectedSegmentTintColor = segmentColor
            segmentedControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
            segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .selected)
            segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .highlighted)
        }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        performSearch()
    }
    //PARAT:3
    func iTunesURL(searchRext: String, category: Int) -> URL {
        
        let kind: String
        switch category {
        case 1: kind = "musicTrack"
        case 2: kind = "software"
        case 3: kind = "ebook"
        default: kind = ""
    }
        
        let encodedText = searchRext.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "https://itunes.apple.com/search?term=\(encodedText)&limit=200&entity=\(kind)"
        let url = URL(string: urlString)
       //showNetworkError()
        return url!
    }
    
    func parse(data: Data) -> [SearchResult] {
        do{
            let decoder = JSONDecoder()
            let result = try decoder.decode(ResultArray.self, from: data)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return[]
        }
    }
     
    func showNetworkError() {
        let alert = UIAlertController(title: "Whooops...", message: "There was an error accessing the iTunes Store." + "Please try again.", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    }

    extension ViewController: UISearchBarDelegate {
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            performSearch()
        }
        
        func performSearch() {
            //PART: 3
           if !searchBar.text!.isEmpty {
                searchBar.resignFirstResponder()
                
                // Part 4 Code:-
                dataTask?.cancel()
                isLoading = true
                tableView.reloadData()
                // End New Code.
                
                hasSearched = true
                searchResults = []
                
                //Replaceing Codes:-
                // 1
            let url = iTunesURL(searchRext: searchBar.text!, category:  segmentedControl.selectedSegmentIndex)
                // 2
                let session = URLSession.shared
                // 3
                dataTask = session.dataTask(with: url, completionHandler: { data, response, error in
                // 4
                    if let error = error as NSError?, error.code == -999 {
                        //print("Failure! \(error.localizedDescription)")
                        return
                    } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                        if let data = data {
                            self.searchResults = self.parse(data: data)
                          //  self.searchResults.sort(by: <)
                            DispatchQueue.main.async {
                                self.isLoading = false
                                self.tableView.reloadData()
                            }
                            return
                        }
                    } else {
                        print("Failure! \(response!)")
                    }
                    DispatchQueue.main.async {
                        self.hasSearched = false
                        self.isLoading = false
                        self.tableView.reloadData()
                        self.showNetworkError()
                    }
                })
                // 5
                dataTask?.resume()
        }
    }
        
        func position(for bar: UIBarPositioning) -> UIBarPosition {
            return.topAttached
        }
    }

    //TableView Methods:-
    extension ViewController: UITableViewDelegate, UITableViewDataSource {
       
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if isLoading {
                return 1
            }else if !hasSearched {
                return 0
            }else if searchResults.count == 0 {
                return 1
            }else {
            return searchResults.count
        }
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            //New Code For Loading Cell:-
            if isLoading {
                let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.loadingCell, for: indexPath)
                
                let spinner = cell.viewWithTag(100) as! UIActivityIndicatorView
                spinner.startAnimating()
                return cell
            }else
            //New Code For Not Found:-
            if searchResults.count == 0 {
                return tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.nothingFoundCell, for: indexPath)
            }
            // For Find Results:-
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.searchResultCell, for: indexPath) as! SearchResultCell
                
                let searchResult = searchResults[indexPath.row]
                cell.nameLabel.text = searchResult.name
               // cell.artistNameLabel.text = searchResult.artistName
                if searchResult.artist.isEmpty {
                    cell.artistNameLabel.text = "Unknown"
                } else {
                    cell.artistNameLabel.text = String(format: "%@ (%@)", searchResult.artist, searchResult.type)
                }
                return cell
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
            if searchResults.count == 0 || isLoading{
                return nil
            }else {
                return indexPath
            }
       }
    }

